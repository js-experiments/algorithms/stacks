const observable = factory => (...args) => {
  const observers = [];
  const obj = factory(...args);
  const notify = (event, ...args) =>
    observers
      .filter(o => o.event === event)
      .forEach(o => o.callback.apply(obj, [event, ...args]));

  return Object.assign(
    {
      subscribe(event, callback) {
        observers.push({
          event,
          callback,
        });
      },
      notify,
    },
    obj,
  );
};

const node = data => ({
  data,
  next: null,
});

const stack = observable(() => {
  const createSubStack = function() {
    const subStack = stack();
    this.notify('substack', subStack);

    return subStack;
  };

  return {
    nodes: null,
    push(node) {
      this.nodes = {
        ...node,
        next: this.nodes,
      };

      this.notify('push', node);
    },

    pop() {
      if (this.nodes === null) {
        return null;
      }

      const node = {
        ...this.nodes,
      };

      // Return only the node, not the whole stack
      node.next = null;

      // update the stack
      this.nodes = this.nodes ? this.nodes.next : null;

      this.notify('pop', node);
      return node;
    },

    peek() {
      if (this.isEmpty()) {
        return null;
      }

      const node = {
        ...this.nodes,
        next: null,
      };

      return node;
    },

    size() {
      let aux = this.nodes;

      let count = 0;

      while (aux !== null) {
        count++;
        aux = aux.next;
      }

      return count;
    },

    isEmpty() {
      return this.nodes === null;
    },

    sort() {
      const ordered = createSubStack.apply(this, []);

      while (!this.isEmpty()) {
        const toSort = this.pop();
        const {
          data: {value},
        } = toSort;

        this.notify('hold', toSort);
        while (!ordered.isEmpty() && ordered.peek().data.value > value) {
          this.push(ordered.pop());
        }

        this.notify('release', toSort);
        ordered.push(toSort);
      }

      this.nodes = {
        ...ordered,
      };
    },
  };
});

const createStackElement = () => {
  const modifiers = ['--top', '--bottom', '--front', '--back', '--left', '--right'];
  const mainElement = document.createElement('div');
  mainElement.classList.add('box');
  const faces = Array.from(Array(6), (item, index) => {
    const face = document.createElement('div');
    face.classList.add('box__face', `box__face${modifiers[index]}`);
    mainElement.appendChild(face);

    return face;
  });

  return {
    mainElement,
    faces
  };
};

const stackRenderer = observable((stack, toAdd, mainElm = null) => {
  // DOM Elements
  const stacksElement = mainElm || document.getElementById('stacks');
  const speedElement = document.getElementById('speed');
  const stackContainer = document.createElement('section');
  const statusElement = document.createElement('h2');
  const stackElement = document.createElement('div');
  const peekElement = document.createElement('div');

  let toAnimate = [];
  let intervalId;
  let speed = +speedElement.value;
  let nextAnimation;

  speedElement.addEventListener('change', () => {
    clearInterval(intervalId);
    speed = (+speedElement.max + +speedElement.min) - +speedElement.value;
    render(nextAnimation);
  });

  const createNodeElement = value => {
    const nodeElement = createStackElement();
    nodeElement.mainElement.classList.add(`box--${value}`);

    return nodeElement;
  };

  const onPushPop = (type, node, container = null) => {
    toAnimate.push({
      node,
      type,
      container,
    });
  };

  const onSubTask = (event, stack) => {
    const subStackContainer = document.createElement('div');
    subStackContainer.classList.add('substack', 'stack__elements');
    stackContainer.appendChild(subStackContainer);

    stack.subscribe('push', (e, node) => onPushPop(e, node, subStackContainer));
    stack.subscribe('pop', (e, node) => onPushPop(e, node, subStackContainer));
    stack.subscribe('substack', (e, subStack) => onSubTask(e, subStack));
  };

  const removeElement = e => e.target.remove();

  const render = function(next) {
    nextAnimation = next;
    intervalId = setInterval(() => {
      const animation = toAnimate.shift();

      if (!animation) {
        // clearInterval(intervalId);
        if (next) {
          return next.call(this);
        }

        return;
      }
      const {
        container,
        node: {
          data: {
            element: nodeElement,
            value,
          },
        },
        type,
      } = animation;
      const {mainElement: element} = nodeElement;
      let statusDiv = statusElement;
      let stackDiv = container || stackElement;

      element.removeEventListener('animationend', removeElement);

      switch (type) {
        case 'hold':
          element.classList.add('.--on-hold');
          element.style.animation = 'animateHold 300ms';
          peekElement.insertBefore(element, peekElement.firstChild);
          break;
        case 'release':
          element.style.animation = 'animateOut 300ms';
          element.addEventListener('animationend', removeElement);
          break;
        case 'pop':
          statusElement.textContent = `Popping element ${value}`;
          element.style.animation = 'animateOut 300ms';
          element.addEventListener('animationend', removeElement);
          break;
        default:
          element.style.animation = 'animateIn 300ms';
          statusElement.textContent = `Pushing element ${value}`;
          stackDiv.insertBefore(element, stackDiv.firstChild);
      }
    }, speed);
  };

  // Subscribe to stack changes
  stack.subscribe('push', (e, node) => onPushPop(e, node));
  stack.subscribe('pop', (e, node) => onPushPop(e, node));
  stack.subscribe('hold', (e, node) => onPushPop(e, node));
  stack.subscribe('release', (e, node) => onPushPop(e, node));
  stack.subscribe('substack', (e, subStack) => onSubTask(e, subStack));

  return {
    renderPushPop() {
      toAdd.forEach(i =>
        stack.push(
          node({
            value: i,
            element: createNodeElement(i),
          }),
        ),
      );
      toAdd.forEach(i => stack.pop());

      return this;
    },

    renderPushSort() {
      toAdd.forEach(i =>
        stack.push(
          node({
            value: i,
            element: createNodeElement(i),
          }),
        ),
      );

      this.renderSort();

      return this;
    },

    renderSort() {
      stack.sort();

      return this;
    },

    start(next) {
      stackContainer.classList.add('stack');
      statusElement.classList.add('stack__status');
      peekElement.classList.add('stack__peek');
      stackElement.classList.add('stack__elements');
      // stackContainer.appendChild(statusElement);
      stackContainer.appendChild(peekElement);
      stackContainer.appendChild(stackElement);
      stacksElement.appendChild(stackContainer);
      render.apply(this, [next]);

      // Clean stack in a ugly way ;)
      stack.nodes = null;

      return this;
    },

    stop() {
      toAnimate = [];
      clearInterval(intervalId);
      statusElement.remove();
      peekElement.remove();
      stackElement.innerHTML = '';
      stackElement.remove();
      stackContainer.innerHTML = '';
      stackContainer.remove();

      return this;
    },
  };
});

const newStack = stack();
const toAdd = ['vue', 'angular', 'react', 'vue', 'vue', 'react', 'angular', 'react', 'react', 'angular', 'vue', 'react'];

const renderer = stackRenderer(newStack, toAdd);

const menu = document.getElementById('stacks-menu');
const menuItems = menu.querySelectorAll('li');
const onClickMenuItem = function(e) {
  const action = this.dataset.renderer;
  renderer
    .stop()
    .start()
    [action]();
};

menuItems.forEach(item => item.addEventListener('click', onClickMenuItem));
